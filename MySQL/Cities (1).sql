-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 13, 2018 at 10:34 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Cities`
--

-- --------------------------------------------------------

--
-- Table structure for table `Cafe`
--

CREATE TABLE `Cafe` (
  `idCafe` int(11) NOT NULL,
  `idCity` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Pictures` text NOT NULL,
  `Wifi` text NOT NULL,
  `Plugs` text NOT NULL,
  `Reviews` text NOT NULL,
  `Distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Cafe`
--

INSERT INTO `Cafe` (`idCafe`, `idCity`, `Name`, `Pictures`, `Wifi`, `Plugs`, `Reviews`, `Distance`) VALUES
(1, 1, 'Alsur Cafe', 'yes', 'yes', 'no', 'yes', 0),
(2, 1, 'Library Cafe', 'yes', 'yes', 'yes', 'yes', 0),
(5, 7, 'Nomad Roasters Home', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Cities`
--

CREATE TABLE `Cities` (
  `idCity` int(11) NOT NULL,
  `Name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Cities`
--

INSERT INTO `Cities` (`idCity`, `Name`) VALUES
(1, 'Barcelona'),
(2, 'Washington D.C.'),
(6, 'Amsterdam'),
(7, 'Madrid'),
(8, 'Milan');

-- --------------------------------------------------------

--
-- Table structure for table `Countries`
--

CREATE TABLE `Countries` (
  `Code` varchar(5) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Countries`
--

INSERT INTO `Countries` (`Code`, `Name`) VALUES
('ITA', 'Italy'),
('US', 'United States'),
('SP', 'Spain'),
('ENG', 'England'),
('IR', 'Ireland'),
('IR', 'Ireland'),
('', 'italy');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `idUser` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Surname` varchar(50) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Cafe`
--
ALTER TABLE `Cafe`
  ADD PRIMARY KEY (`idCafe`);

--
-- Indexes for table `Cities`
--
ALTER TABLE `Cities`
  ADD PRIMARY KEY (`idCity`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Cafe`
--
ALTER TABLE `Cafe`
  MODIFY `idCafe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `Cities`
--
ALTER TABLE `Cities`
  MODIFY `idCity` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
